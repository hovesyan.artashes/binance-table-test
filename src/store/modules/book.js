import Binance from 'binance-api-node'
const client = Binance();
import {parseWsData} from '../utils/index.js'

export default {
  actions: {
    async fetchBook(ctx,symbol){
      const book = await client.book({ symbol: symbol });
      ctx.commit('SET_BOOK',book)
    },
    async subscribeBook(ctx,symbol){
      let binanceSocket = new WebSocket(`wss://stream.binance.com:9443/ws/${symbol.toLowerCase()}@depth@100ms`);
      ctx.commit('SET_ACTIVE_SOCKET',binanceSocket);
      binanceSocket.onmessage = (event) => {
        let jsonData = JSON.parse(event.data);
        ctx.commit('UPDATE_BIDS',parseWsData(jsonData.b));
        ctx.commit('UPDATE_ASKS',parseWsData(jsonData.a));
      };
    },

  },
  mutations: {
    SET_BOOK(state,book){
      state.bids = book.bids;
      state.asks = book.asks;
    },
    UPDATE_BIDS(state,payload){
      state.bids = payload.concat(state.bids);
      state.bids = state.bids.slice(0,100);
    },
    UPDATE_ASKS(state,payload){
      state.asks = payload.concat(state.asks);
      state.asks = state.asks.slice(0,100)
    },
    SET_ACTIVE_SOCKET(state,socket){
      if(state.active_socket){
        state.active_socket.close(); //close connection
      }
      state.active_socket = socket;
    }
  },
  state: {
    bids: [],
    asks: [],
    active_socket: null
  },
  getters: {
    getBids(state) {
      return state.bids;
    },
    getAsks(state) {
      return state.asks;
    }
  },
}
