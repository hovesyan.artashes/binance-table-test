export function parseWsData(array){
  return array.filter(obj => {
    if(parseFloat(obj[1]) !== 0) {
      return obj;
    }
  }).map(item => {
    return {
      price: item[0],
      quantity: item[1]
    };
  });
}
